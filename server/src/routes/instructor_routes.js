// import express module for router
const express = require('express');

// import instructor controller object for instructor controller methods
const instructorController = require('../controller/instructor_controller.js');

const router = express.Router();

// export instructor routes
export default router;
